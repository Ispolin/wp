<?php
/*
Plugin Name: API Bearer Auth
Plugin URI: http://www.michielvaneerd.nl/api-bearer-auth
Description: Authentication for REST API
Text Domain: api_bearer_auth
Version: 20171208
Author: Michiel van Eerd
License: GPL2
*/

// Always update this!
define('API_BEARER_AUTH_PLUGIN_VERSION', '20171208');

/**
 * How long access token will be valid.
 */
if (!defined('API_BEARER_ACCESS_TOKEN_LENGTH_UNIT')) {
  // Possible values: DAY, WEEK, MONTH, YEAR
  define('API_BEARER_ACCESS_TOKEN_LENGTH_UNIT', 'DAY');
}
if (!defined('API_BEARER_ACCESS_TOKEN_LENGTH')) {
  define('API_BEARER_ACCESS_TOKEN_LENGTH', 1);
}
/**
 * How long refresh token will be valid.
 */
if (!defined('API_BEARER_REFRESH_TOKEN_LENGTH_UNIT')) {
  // Possible values: DAY, WEEK, MONTH, YEAR
  define('API_BEARER_REFRESH_TOKEN_LENGTH_UNIT', 'YEAR');
}
if (!defined('API_BEARER_REFRESH_TOKEN_LENGTH')) {
  define('API_BEARER_REFRESH_TOKEN_LENGTH', 10);
}

if (!class_exists('API_Bearer_Auth')) {

  require_once(__DIR__ . '/db.php');

  class API_Bearer_Auth
  {

  const PASSWORD = '3sc3RLrpd17';
  const METHOD = 'aes-256-cbc';

    private $db;

    /**
     * Our URLS that don't require authentication.
     * If you use this plugin, all calls to the REST API are blocked for unauthenticated users.
     * Use the api_bearer_auth_unauthenticated_urls filter to add other URLS that don't require authentication.
     */
    private static $UNAUTHENTICATED_ENDPOINTS = [
      'POST' => [
        "/wp-json/api-bearer-auth/v1/login/?",
        "/wp-json/api-bearer-auth/v1/tokens/refresh/?",
          "/wp-json/api-bearer-auth/v1/update/?",
          "/wp-json/api-bearer-auth/v1/found/?",
          "/wp-json/api-bearer-auth/v1/change/?",
          "/wp-json/api-bearer-auth/v1/crypt/?",
      ]
    ];

      /**
       * API_Bearer_Auth constructor.
       */
      function __construct() {
      $this->db = new API_Bearer_Auth_Db();
      add_filter('determine_current_user', [$this, 'determine_current_user_filter']);
      add_filter('rest_authentication_errors', [$this, 'rest_authentication_errors_filter']);
      add_action('rest_api_init', [$this, 'rest_api_init_action']);
      add_action('deleted_user', [$this, 'deleted_user_action']);
      if (is_admin()) {
        // We don't use the activation_hook, because this is not called for mu plugins.
        add_action('plugins_loaded', [$this, 'admin_plugins_loaded_action']);
      }
    }

    public function admin_plugins_loaded_action() {
      global $wpdb;
      // This is nicer, but also less performant, so make use of a define of the version
      //require_once(ABSPATH . 'wp-admin/includes/plugin.php');
      //$data = get_plugin_data(__FILE__, false, false);
      //$data = get_file_data(__FILE__, ['Version' => 'Version']);
      if (get_option('api_bearer_auth_activated') != API_BEARER_AUTH_PLUGIN_VERSION) {
        self::on_activation();
        update_option('api_bearer_auth_activated', API_BEARER_AUTH_PLUGIN_VERSION, false);
      }
    }
 
    /**
     * Action fired when user is deleted. Deletes tokens for this user.
     * @param integer $id User id.
     */ 
    public function deleted_user_action($id) {
      $this->db->delete_by_user_id($id);
    }
  
    /**
     * Code runs on activation of this plugin. We create / upgrade the tokens table.
     */
    public static function on_activation() {
      global $wpdb;
      $tableName = $wpdb->base_prefix . 'user_tokens';
      $charsetCollate = $wpdb->get_charset_collate();
  
      // https://codex.wordpress.org/Creating_Tables_with_Plugins
      // There are VERY specific requirements when you use dbDelta
      $sql = "CREATE TABLE $tableName (
        id int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
        user_id int(11) UNSIGNED NOT NULL,
        access_token varchar(255) NOT NULL,
        access_token_valid datetime NOT NULL,
        refresh_token varchar(255) NOT NULL,
        refresh_token_valid datetime NOT NULL,
        PRIMARY KEY  (id),
        UNIQUE KEY key_user_id_token (user_id)
      ) $charsetCollate;";
      
      require_once(ABSPATH . 'wp-admin/includes/upgrade.php');
      dbDelta($sql);
    }
  
    /**
     * Code runs on uninstall. Drop table.
     */
    public static function on_uninstall() {
      global $wpdb;
      delete_option('api_bearer_auth_activated');
      $wpdb->query('DROP TABLE ' . $wpdb->base_prefix . 'user_tokens');
    }
  
    /**
     * By default, this filter determines the current user from the cookie.
     * We use it to determine the current user from the access token in the Authorization header.
     * If no Authorization header exists, just return the default result.
     */
    public function determine_current_user_filter($user_id) {
  
      // If we use wp-cli we have no headers to look for
      if (php_sapi_name() === 'cli') {
        return $user_id;
      }
 
      /**
       * Make sure to add the lines below to .htaccess
       * otherwise Apache may strip out the auth header.
       * RewriteCond %{HTTP:Authorization} ^(.*)
       * RewriteRule ^(.*) - [E=HTTP_AUTHORIZATION:%1]
       */
      $headers = function_exists('apache_request_headers')
        ? apache_request_headers() : $_SERVER;
      $possibleAuthHeaderKeys = ['Authorization', 'HTTP_AUTHORIZATION', 'REDIRECT_HTTP_AUTHORIZATION'];
      $authHeader = null;
      foreach ($possibleAuthHeaderKeys as $key) {
        if (!empty($headers[$key])) {
          $authHeader = $headers[$key];
          break;
        }
      }
      
      if (!empty($authHeader)) {
        // 7 = strlen('Bearer ');
        $access_token = substr($authHeader, 7);
        $my_user_id = $this->db->get_user_id_from_access_token($access_token);
        if (!empty($my_user_id)) {
          return $my_user_id;
        }
        return false;
      }
      return $user_id;
    }
  
    /**
     * Check if we are allowed to do this API request.
     */
    public function rest_authentication_errors_filter($error) {
  
      // If $error is not empty, another auth method has set this
      // so we don't need to do anything.
      if (!empty($error)) {
        return $error;
      }
  
      if (!is_user_logged_in()) {

        // Strip out query string
        $currentUrl = (!empty($_SERVER['HTTPS']) ? 'https' : 'http') . '://' . $_SERVER['HTTP_HOST'] . strtok($_SERVER['REQUEST_URI'], '?');
        $siteUrl = get_site_url();

        $custom_urls = [];
        /**
         * Filter: api_bearer_auth_unauthenticated_urls
         * Add URLs that should be avialble to unauthenticated users.
         * Specify only the part after the site url, e.g. /wp-json/wp/v2/users
         * Each URL will be prepended by the value of get_site_url()
         * And each resulting URL will be put in between ^ and $ regular expression signs.
         */
        $custom_urls = apply_filters('api_bearer_auth_unauthenticated_urls', $custom_urls, $_SERVER['REQUEST_METHOD']);
        $my_urls = [];
        if (!empty(self::$UNAUTHENTICATED_ENDPOINTS[$_SERVER['REQUEST_METHOD']])) {
          $my_urls = self::$UNAUTHENTICATED_ENDPOINTS[$_SERVER['REQUEST_METHOD']];
        }
        $urls = array_merge($custom_urls, $my_urls);
        $found = false;
        foreach ($urls as $url) {
          if (preg_match('@^' . $siteUrl . $url . '$@', $currentUrl)) {
            $found = true;
            break;
          }
        }
        if (!$found) {
          return new WP_Error('api_bearer_auth_not_logged_in',
            __('You are not logged in.', 'api_bearer_auth'), ['status' => 401]);
        } else {
          // We did not authenticate this user (because we have no user).
          // But this request is allowed publicly for us.
          // So return default (null), maybe other filter callbacks will do something with this.
          return $error;
        }
      } elseif (!is_user_member_of_blog()) {
        return new WP_Error('api_api_bearer_auth_wrong_blog',
          __('You are no member of this blog.', 'api_bearer_auth'), ['status' => 401]);
      }
      
      // We have and authenticated user that is member of this blog and we have no problem with this request.
      // But maybe other filters have, so return $error (that will be null)
      return $error;

    }
  
    /**
     * Registering the routes endpoints
     */
    public function rest_api_init_action()
    {
        register_rest_route('api-bearer-auth/v1', '/login', [
        'methods' => 'POST',
        'callback' => [$this, 'callback_login'],
//        'args' => [
//          'username' => [
//            'required' => true,
//          ],
//          'password' => [
//            'required' => true,
//          ],
//        ]
      ]);

        register_rest_route('api-bearer-auth/v1', '/tokens/refresh', [
        'methods' => 'POST',
        'callback' => [$this, 'callback_refresh_token'],
        'args' => [
          'token' => [
            'required' => true,
            'validate_callback' => function($param, $request, $key) {
              return API_Bearer_Auth_Db::validate_refresh_token_format($param);
            }
          ]
        ]
      ]);

        register_rest_route('api-bearer-auth/v1', '/update', [
            'methods' => 'POST',
            'callback' => [$this, 'update'],
        ]);

        register_rest_route('api-bearer-auth/v1', '/found', [
            'methods' => 'POST',
            'callback' => [$this, 'found'],
        ]);

        register_rest_route('api-bearer-auth/v1', '/change', [
            'methods' => 'POST',
            'callback' => [$this, 'change'],
        ]);

        register_rest_route('api-bearer-auth/v1', '/crypt', [
            'methods' => 'POST',
            'callback' => [$this, 'crypt'],
        ]);
    }

    public function crypt(WP_REST_Request $request)
    {
        $body = $request->get_json_params();
        $text = $this->$body['text'];
        return rest_ensure_response([
            'encrypt' => $this->encrypt($text)
        ]);
    }

      /**
       * look user by mail
       *
       * @param WP_REST_Request $request
       * @return mixed|WP_REST_Response
       */
      public function found(WP_REST_Request $request)
    {
        global $wpdb;
        $body = $request->get_json_params();

          $email = $this->decrypt($body['email']);
        $query = "SELECT * FROM wpjq_users WHERE user_email = '$email'";
        $results = $wpdb->get_row( $query , OBJECT );
        if (!$results) {
            return rest_ensure_response([
                'found' => "",
                'tokens' => "",
                'key' => ""
            ]);
        } else {
            $accesToken = rand(1111111, 9999999);
            $wpdb->update( 'wpjq_users',array(
                'user_key' => $accesToken

            ),array('user_email' => $email));

            return rest_ensure_response([
                'found' => true,
                'tokens' => $this->encrypt($results->user_tokens),
                'key' => $this->encrypt($accesToken)
            ]);
        }

    }


      public function callback_login(WP_REST_Request $request) {
      
      $body = $request->get_json_params();

      $username = $this->decrypt($body['username']);
      $password = $this->decrypt($body['password']);
      $user = wp_authenticate($username, $password);
      if (is_wp_error($user)) {
        return rest_ensure_response($user);
      }
      if (!is_user_member_of_blog($user->ID)) {
        return new WP_Error('api_api_bearer_auth_wrong_blog',
          __('You are no member of this blog.', 'api_bearer_auth'), ['status' => 401]);
      }
      // Update access and refresh tokens
      if (($result = $this->db->login($user->ID)) !== false) {
          $tokens = $this->encrypt($user->data->user_tokens);

        return rest_ensure_response([
          'name' => $user->data->display_name,
            'hyuxheihumeufop' => $tokens,
          'user' => $result['access_token'],
//          'refresh_token' => $result['refresh_token'],
        ]);
      }
      // WP user ophalen wel, maar Access token aanmaken niet gelukt
      return rest_ensure_response(new WP_Error('api_api_bearer_auth_create_token',
        __('Error creating tokens.', 'api_api_bearer')));
    }

      /**
       * change scores
       *
       * @param WP_REST_Request $request
       * @return mixed|WP_REST_Response
       */
      public function change(WP_REST_Request $request)
      {
          global $wpdb;

          $body = $request->get_json_params();

          $score = $this->decrypt($body['tokens']);
          $key = $this->decrypt($body['key']);
          $query = "SELECT * FROM wpjq_users WHERE user_key = '$key'";
          $results = $wpdb->get_row( $query , OBJECT );

          if ($results) {
              $wpdb->update( 'wpjq_users',array(
                  'user_tokens' => $score,
                  'user_key' => 0
              ),array('user_key' => $key));

              return rest_ensure_response([
                  'success' => true
              ]);
          } else {
              return rest_ensure_response([
                  'success' => false
              ]);
          }
      }

      /**
       * api update
       *
       * @param WP_REST_Request $request
       * @return mixed|WP_REST_Response
       */
      public function update(WP_REST_Request $request)
      {
          global $wpdb;

          $body = $request->get_json_params();

          $score = $this->decrypt($body['hyuxheihumeufop']);
          $user = $this->decrypt($body['user']);
          $headers = apache_request_headers();
          $authorization = $headers['Authorization'];

          $token = explode(' ', $authorization);
          if ($user == $token) {
              $userInfo = $this->db->get_user_id_from_access_token($token[1]);
              $wpdb->update( 'wpjq_users',array(
                  'user_tokens' => $score

              ),array('ID' => $userInfo));

              return rest_ensure_response([
                  'success' => true
              ]);

//        get information
//          $query = "SELECT * FROM wp_users WHERE `ID` = $userInfo";
//          $results = $wpdb->get_row( $query , OBJECT );

          } else {
              return rest_ensure_response([
                  'success' => false
              ]);
          }
      }


      /**
       * encrypt information
       *
       * @param $string
       * @return string
       */
      public function encrypt($string)
    {
        $password = substr(hash('sha256', self::PASSWORD, true), 0, 32);
        $iv =
            chr(0x0).
            chr(0x0).
            chr(0x0).
            chr(0x0).
            chr(0x0).
            chr(0x0).
            chr(0x0).
            chr(0x0).
            chr(0x0).
            chr(0x0).
            chr(0x0).
            chr(0x0).
            chr(0x0).
            chr(0x0).
            chr(0x0).
            chr(0x0);
        $encryptedString = base64_encode(openssl_encrypt($string, self::METHOD, $password, OPENSSL_RAW_DATA, $iv));
        return $encryptedString;
    }

      /**
       * decrypt information
       *
       * @param $encryptedString
       * @return string
       */
      public function decrypt($encryptedString)
    {
        $password = substr(hash('sha256', self::PASSWORD, true), 0, 32);
        $iv = chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0);
        $decryptedString = openssl_decrypt(base64_decode($encryptedString), self::METHOD, $password, OPENSSL_RAW_DATA, $iv);
        return $decryptedString;
    }
  }

 
  //register_activation_hook(__FILE__, ['API_Bearer_Auth', 'on_activation']);
  register_uninstall_hook(__FILE__, ['API_Bearer_Auth', 'on_uninstall']);

  new API_Bearer_Auth();
}

