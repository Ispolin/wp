<?php

class API_Bearer_Auth_Db {

  private static $TOKEN_BYTE_LENGTH = 32;

  /*
   * Returns WP user id from access token.
   * @param $access_token Access token of user
   * @return integer|null WP user is or null if nothing is found.
   */
  public function get_user_id_from_access_token($access_token) {
    global $wpdb;
    return $wpdb->get_var($wpdb->prepare('SELECT user_id
      FROM ' . $wpdb->base_prefix . 'user_tokens
      WHERE access_token = %s AND access_token_valid > NOW()', $access_token));
  }

  /**
   * Remove token for this user.
   * @param integer $id User id.
   * @return void
   */
  public function delete_by_user_id($id) {
    global $wpdb;
    $wpdb->delete($wpdb->base_prefix . 'user_tokens', [
      'user_id' => $id
    ]);
  }

  /**
   * Refresh the access token for the user that belongs to this refresh token.
   * @param $refresh_token Refresh token of this user.
   * @return array|false Array with new access token or false on error.
   */
  public function refresh_access_token($refresh_token) {
    global $wpdb;
    $user_id = $this->get_user_id_from_refresh_token($refresh_token);
    if (!empty($user_id)) {
      $access_token = $this->create_access_token();
      if ($wpdb->update($wpdb->base_prefix . 'user_tokens', [
          'access_token' => $access_token
        ], [
          'user_id' => $user_id
        ]) !== false)
      {
        return [
          'access_token' => $access_token
        ];
      }
    }
    return false;
  }

  /**
   * Returns user id for this refresh token.
   * @param string $refresh_token Refresh token.
   * @return integer|null User id or null when nothing is found.
   */
  public function get_user_id_from_refresh_token($refresh_token) {
    global $wpdb;
    return $wpdb->get_var($wpdb->prepare('SELECT user_id
      FROM ' . $wpdb->base_prefix . 'user_tokens
      WHERE refresh_token = %s AND refresh_token_valid > NOW()', $refresh_token));
  }

  /*
   * Inserts or update access and refresh tokens for user after login.
   * @param $user_id ID of WP user
   * @return false on error or array with access and refresh token on success
   */
  public function login($user_id) {
    global $wpdb;
    $access_token = $this->create_access_token();
    $refresh_token = $this->create_refresh_token();
    if ($wpdb->query($wpdb->prepare("INSERT INTO " . $wpdb->base_prefix . "user_tokens
      SET
      user_id = %d,
      access_token = %s,
      access_token_valid = DATE_ADD(NOW(), INTERVAL "
          . API_BEARER_ACCESS_TOKEN_LENGTH . " "
          . API_BEARER_ACCESS_TOKEN_LENGTH_UNIT . "),
      refresh_token = %s,
      refresh_token_valid = DATE_ADD(NOW(), INTERVAL "
          . API_BEARER_REFRESH_TOKEN_LENGTH . " "
          . API_BEARER_REFRESH_TOKEN_LENGTH_UNIT . ")
      ON DUPLICATE KEY UPDATE
      access_token = %s,
      access_token_valid = DATE_ADD(NOW(), INTERVAL "
          . API_BEARER_ACCESS_TOKEN_LENGTH . " "
          . API_BEARER_ACCESS_TOKEN_LENGTH_UNIT . "),
      refresh_token = %s,
      refresh_token_valid = DATE_ADD(NOW(), INTERVAL "
          . API_BEARER_REFRESH_TOKEN_LENGTH . " "
          . API_BEARER_REFRESH_TOKEN_LENGTH_UNIT . ")",
      $user_id,
      $access_token,
      $refresh_token,
      $access_token,
      $refresh_token)) !== false)
    {
      return [
        'access_token' => $access_token,
        'refresh_token' => $refresh_token
      ];
    }

    // Error
    return false;

  }

  /*
   * Returns access token.
   */
  private function create_access_token() {
    return bin2hex(openssl_random_pseudo_bytes(self::$TOKEN_BYTE_LENGTH));
  }

  /*
   * Returns refresh token.
   */
  private function create_refresh_token() {
    return bin2hex(openssl_random_pseudo_bytes(self::$TOKEN_BYTE_LENGTH));
  }

  public static function validate_refresh_token_format($token) {
    return preg_match("/^[a-f0-9]{" . self::$TOKEN_BYTE_LENGTH * 2 . "}$/", $token);
  }

  public static function validate_access_token_format($token) {
    return preg_match("/^[a-f0-9]{" . self::$TOKEN_BYTE_LENGTH * 2 . "}$/", $token);
  }

}
