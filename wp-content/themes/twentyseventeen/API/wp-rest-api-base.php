<?php

class ApiBaseController extends WP_REST_Controller {
    //The namespace and version for the REST SERVER
    var $my_namespace = 'api/v';
    var $my_version = '1';
    public function register_routes() {
        $namespace = $this->my_namespace.$this->my_version;

        register_rest_route($namespace, '/found', array(
                array(
                    'methods'  => 'POST',
                    'callback' => array(new ApiDefaultController('found'), 'init'),
                )
            )
        );

        register_rest_route($namespace, '/change', array(
                array(
                    'methods'  => 'POST',
                    'callback' => array(new ApiDefaultController('change'), 'init'),
                )
            )
        );

        register_rest_route($namespace, '/reg', array(
                array(
                    'methods'  => 'POST',
                    'callback' => array(new ApiDefaultController('verification'), 'init'),
                )
            )
        );

//        register_rest_route($namespace, '/sms', array(
//                array(
//                    'methods'  => 'POST',
//                    'callback' => array(new ApiDefaultController('sms'), 'init'),
//                )
//            )
//        );

        register_rest_route($namespace, '/check', array(
                array(
                    'methods'  => 'POST',
                    'callback' => array(new ApiDefaultController('check'), 'init'),
                )
            )
        );
    }
    // Register our REST Server
    public function hook_rest_server() {
        add_action('rest_api_init', array($this, 'register_routes'));
        //add_action('rest_api_init', 'my_customize_rest_cors', 15);
    }
    public function my_customize_rest_cors() {
        remove_filter('rest_pre_serve_request', 'rest_send_cors_headers');
        remove_filter('rest_post_dispatch', 'rest_send_allow_header');
    }
}

$ApiBaseController = new ApiBaseController();
$ApiBaseController->hook_rest_server();