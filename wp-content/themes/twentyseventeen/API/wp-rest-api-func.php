<?php

class ApiDefaultController extends ApiBaseController
{
    public $method;
    public $response;

    const PASSWORD = '3sc3RLrpd17';
    const METHOD = 'aes-256-cbc';

    const TWILIO_API_KEY = "QO7CDakcuIytOCI1FLAM1vcKHgozKXrP";
    const TWILIO_SMS_URL = "https://api.authy.com/protected/json/phones/verification/start";
    const TWILIO_CHECK_URL = "https://api.authy.com/protected/json/phones/verification/check";

    public function __construct($method)
    {
        $this->method = $method;
//        $this->response = array(
//            'Status' => false,
//            'StatusCode' => 0,
//            'StatusMessage' => 'Default'
//        );
    }

    public function init(WP_REST_Request $request)
    {
        try {
            if (!method_exists($this, $this->method)) {
                throw new Exception('No method exists', 500);
            }
            $data = $this->{$this->method}($request);
//            $this->response['Status'] = $this->status_codes['success'];
//            $this->response['StatusCode'] = 1000;
//            $this->response['status'] = 'success';
            $this->response = $data;
        } catch (Exception $e) {
//            $this->response['Status'] = false;
//            $this->response['StatusCode'] = $e->getCode();
//            $this->response['status'] = $e->getMessage();
//            $this->response = $data;
        }

        return $this->response;
    }

    public function found($request)
    {
        global $wpdb;
        $body = $request->get_json_params();

        $email = $this->decrypt($body['email']);
        $query = "SELECT * FROM wpjq_game_users WHERE user_email = '$email'";
        $results = $wpdb->get_row( $query , OBJECT );
        if (!$results) {
            return rest_ensure_response([
                'found' => "",
                'tokens' => "",
                'key' => ""
            ]);
        } else {
            $accesToken = rand(1111111, 9999999);
            $wpdb->update( 'wpjq_game_users',array(
                'user_key' => $accesToken

            ),array('user_email' => $email));

            return rest_ensure_response([
                'found' => true,
                'tokens' => $this->encrypt($results->user_tokens),
                'key' => $this->encrypt($accesToken)
            ]);
        }
//        $data = $request->get_json_params();

//        return $data;
    }

    public function change($request)
    {
        global $wpdb;

        $body = $request->get_json_params();

        $score = $this->decrypt($body['tokens']);
        $key = $this->decrypt($body['key']);
        $query = "SELECT * FROM wpjq_game_users WHERE user_key = '$key'";
        $results = $wpdb->get_row( $query , OBJECT );

        if ($results) {
            $wpdb->update( 'wpjq_game_users',array(
                'user_tokens' => $score,
                'user_key' => 0
            ),array('user_key' => $key));

            return rest_ensure_response([
                'success' => true
            ]);
        } else {
            return rest_ensure_response([
                'success' => false
            ]);
        }
    }

    /**
     * Check duplication in db and send verification code
     *
     * @param $request
     * @return mixed|WP_REST_Response
     */
    public function verification($request)
    {
        global $wpdb;
        $body = $request->get_params();
        $email = $body['email'];
        $phone = $body['phone'];

        $query = "SELECT * FROM wpjq_game_users WHERE user_email = '$email'";
        $user = $wpdb->get_row($query, OBJECT);

        if ($user) {
            return rest_ensure_response([
                'success' => false,
                'message' => 'user with such mail exists'
            ]);
        } else {
            $query = "SELECT * FROM wpjq_game_users WHERE user_phone = '$phone'";
            $user = $wpdb->get_row($query, OBJECT);

            if ($user) {
                return rest_ensure_response([
                    'success' => false,
                    'message' => 'user with such phone exists'
                ]);
            } else {
                $sms = $this->sendSms($body['country_code'], $body['phone']);
                if ($sms) {
                    return rest_ensure_response([
                        'success' => true,
                        'message' => 'sms send'
                    ]);
                } else {
                    return rest_ensure_response([
                        'success' => false,
                        'message' => 'sms not send'
                    ]);
                }
            }
        }
    }


    /**
     * Check sms code and register new user
     *
     * @param $request
     * @return mixed|WP_REST_Response
     */
    public function check($request)
    {
        global $wpdb;
        $body = $request->get_params();

        $phone = $body['phone'];
        $countryCode = $body['country_code'];
        $code = $body['code'];

        $host = self::TWILIO_CHECK_URL;
        $key = self::TWILIO_API_KEY;
        $getParameters = "?country_code=$countryCode&phone_number=$phone&verification_code=$code&api_key=$key";

        $CR = curl_init();
        curl_setopt($CR, CURLOPT_URL, $host.$getParameters);
        curl_setopt($CR, CURLOPT_RETURNTRANSFER, 1);
        curl_exec( $CR );
        $response = curl_getinfo($CR );
        curl_close($CR);

        if ($response['http_code'] == 200) {
            $user = $wpdb->insert('wpjq_game_users', array(
//                'user_pass' => wp_hash_password($body['password']),
                'user_email' => $body['email'],
                'user_phone' => $body['phone'],
                'user_country_code' => $body['country_code'])
            );
            if ($user) {
                return rest_ensure_response([
                    'success' => true,
                    'message' => 'user is registered'
                    ]
                );
            } else {
                return rest_ensure_response([
                        'success' => false,
                        'message' => 'something went wrong try again'
                    ]
                );
            }
        } else {
            return rest_ensure_response([
                    'success' => false,
                    'message' => 'code or number is incorrect'
                ]
            );
        }
    }


    public function location_based_notify($request)
    {
        $data = array();
        return $data;
    }

    /**
     * encrypt information
     *
     * @param $string
     * @return string
     */
    public function encrypt($string)
    {
        $password = substr(hash('sha256', self::PASSWORD, true), 0, 32);
        $iv =
            chr(0x0).
            chr(0x0).
            chr(0x0).
            chr(0x0).
            chr(0x0).
            chr(0x0).
            chr(0x0).
            chr(0x0).
            chr(0x0).
            chr(0x0).
            chr(0x0).
            chr(0x0).
            chr(0x0).
            chr(0x0).
            chr(0x0).
            chr(0x0);
        $encryptedString = base64_encode(openssl_encrypt($string, self::METHOD, $password, OPENSSL_RAW_DATA, $iv));
        return $encryptedString;
    }

    /**
     * decrypt information
     *
     * @param $encryptedString
     * @return string
     */
    public function decrypt($encryptedString)
    {
        $password = substr(hash('sha256', self::PASSWORD, true), 0, 32);
        $iv = chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0) . chr(0x0);
        $decryptedString = openssl_decrypt(base64_decode($encryptedString), self::METHOD, $password, OPENSSL_RAW_DATA, $iv);
        return $decryptedString;
    }

    /**
     * Send sms code
     *
     * @param $code
     * @param $phone
     * @return mixed
     */
    public function sendSms($code, $phone)
    {
        $host = self::TWILIO_SMS_URL;
        $data['country_code'] = $code;
        $data['phone_number'] = $phone;
        $data['via']='sms';
        $data['api_key']=self::TWILIO_API_KEY;


        $postString = '';

        foreach($data AS $key => $val) {
            $postString .= $key . "=" . $val . "&";
        }

        $postString = substr($postString, 0, -1);
        $CR = curl_init();
        curl_setopt($CR, CURLOPT_URL, $host);
        curl_setopt($CR, CURLOPT_POST, 1);
        curl_setopt($CR, CURLOPT_FAILONERROR, true);
        curl_setopt($CR, CURLOPT_POSTFIELDS, $postString);
        curl_setopt($CR, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($CR, CURLOPT_SSL_VERIFYPEER, 0);

//        $httpCode = curl_getinfo($CR, CURLINFO_HTTP_CODE);

        $result = curl_exec( $CR );
//        $error = curl_error( $CR );
        curl_close($CR);

        return $result;
    }
}