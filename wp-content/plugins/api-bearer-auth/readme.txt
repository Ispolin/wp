=== API Bearer Auth ===
Contributors: michielve
Tags: api, rest-api, authentication
Requires at least: 4.6
Tested up to: 4.9.1
Requires PHP: 5.4.0
Stable tag: trunk
License: GPLv2 or later
License URI: https://www.gnu.org/licenses/gpl-2.0.html

Authentication for REST API.

== Description ==

The API Bearer Auth plugin enables authentication for the REST API by using access an refresh tokens.
After a login an access and refresh token is created or updated for the user.

Checkout the [API Bearer Auth website](http://www.michielvaneerd.nl/api-bearer-auth/) with full information.

Endpoints expect JSON in the POST body.

= Login =

Endpoint:

`
POST /api-bearer-auth/v1/login

`

Request body:
`
{"username": "my_username", "password": "my_password"}
`

Response:
`
{
  "wp_user": {
    "data": {
      "ID": 1,
      "user_login": "your_user_login",
      "..."
    }
  },
  "access_token": "your_access_token",
  "refresh_token": "your_refresh_token"
}
`

Make sure to save the access and refresh token!

= Refresh access token =

Endpoint:
`
POST /api-bearer-auth/v1/refresh
`

Request body:
`
{"token": "your_refresh_token"}
`

Response success:
`
{
  "access_token": "your_new_access_token"
}
`

Response when sending a wrong refresh token is a 401:
`
{
  "code": "api_api_bearer_auth_error_invalid_token",
  "message": "Invalid token.",
  "data": {
    "status": 401
  }
}
`

= Do a request =

After you have the access token, you can make requests to authenticated enpoints  with an Authorization header like this:

`
Authorization: Bearer <your_access_token>
`

Apache sometimes strips out the Authorization header. If this is the case, make sure to add this to the .htaccess file:
`
RewriteCond %{HTTP:Authorization} ^(.*)
RewriteRule ^(.*) - [E=HTTP_AUTHORIZATION:%1]
`

If you are not logged in or you send an invalid access token, you get a 401 response:
`
{
  "code": "api_bearer_auth_not_logged_in",
  "message": "You are not logged in.",
  "data": {
    "status": 401
  }
}
`

== Installation ==

1. Upload the plugin files to the `/wp-content/plugins/api-bearer-auth` directory, or install the plugin through the WordPress plugins screen directly.
1. Activate the plugin through the 'Plugins' screen in WordPress


== Frequently Asked Questions ==

= Change period access and refresh tokens are valid =

By default an access token is valid for 1 day and a refresh token for 10 years.
You can change this, by defining the following constants - for example in your wp-config.php file.

`
// DAY, WEEK, MONTH, YEAR are valid values
define('API_BEARER_ACCESS_TOKEN_LENGTH_UNIT', 'DAY');
define('API_BEARER_ACCESS_TOKEN_LENGTH', 10);
define('API_BEARER_REFRESH_TOKEN_LENGTH_UNIT', 'YEAR');
define('API_BEARER_REFRESH_TOKEN_LENGTH', 1);
`

= Whitelist unauthenticated URLs =

By default, when you enable this plugin, all REST API endpoints are only available for authenticated users.
If you want to add some more endpoints to this whitelist, you can use the `api_bearer_auth_unauthenticated_urls` filter.
Note that you need to specify the endpoint relative to the site_url() and that you can specify regular expressions.
For example:

`
add_filter('api_bearer_auth_unauthenticated_urls', 'api_bearer_auth_unauthenticated_urls_filter', 10, 2);
function api_bearer_auth_unauthenticated_urls_filter($custom_urls, $request_method) {
  switch ($request_method) {
    case 'POST':
      $custom_urls[] = '/wp-json/myplugin/v1/something/?';
      break;
    case 'GET':
      $custom_urls[] = '/wp-json/myplugin/v1/something/other/?';
      break;
  }
  return $custom_urls;
}
`

== Changelog ==

= 20171208 =
* Define constants to change valid period of access and refresh tokens

= 20171130 =
* First release

